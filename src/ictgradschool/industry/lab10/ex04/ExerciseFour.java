package ictgradschool.industry.lab10.ex04;

/**
 * Created by anhyd on 27/03/2017.
 */
public class ExerciseFour {

    /**
     * Returns the sum of all positive integers between 1 and num (inclusive).
     */
    public int getSum(int num) {

        // TODO Implement a recursive solution to this method.
        if (num == 0) {
            return num;
        } else {
            return num + getSum(Math.abs(num) - 1);
        }

    }


    /**
     * Returns the smallest value in the given array, between the given first (inclusive) and second (exclusive) indices
     *
     * @param nums        the array
     * @param firstIndex  the inclusive lower index
     * @param secondIndex the exclusive upper index
     */
    public int getSmallest(int[] nums, int firstIndex, int secondIndex) {

        // TODO Implement a recursive solution to this method.
        if ((secondIndex - firstIndex) == 1) {
            return nums[firstIndex];
        } else {
            return Math.min(nums[secondIndex - 1], getSmallest(nums, firstIndex, secondIndex - 1));
        }
    }

    /**
     * Prints all ints from n down to 1.
     */
    public void printNums1(int n) {

        System.out.println(n);
        // TODO Implement a recursive solution to this method.
        if (n > 1) {
            printNums1(n - 1);
        }
    }

    /**
     * Prints all ints from 1 up to n.
     */
    public void printNums2(int n) {

        // TODO Implement a recursive solution to this method.
        if (n > 1) {
            printNums2(n - 1);
        }
        System.out.println(n);
    }

    /**
     * Returns the number of 'e' and 'E' characters in the given String.
     *
     * @param input the string to check
     */
    public int countEs(String input) {

        // TODO Implement a recursive solution to this method.
        int count = 0;
        input = input.toUpperCase();
        if (input.charAt(input.length()-1)=='E') {
            count++;
        }
        if (input.length() > 1) {
            count += countEs(input.substring(0, input.length() - 1));
        }
        return count;
    }

    /**
     * Returns the nth number in the fibonacci sequence.
     */
    public int fibonacci(int n) {

        // TODO Implement a recursive solution to this method.
        if (n == 0) {
            return n;
        } else if (n == 1) {
            return 1;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    /**
     * Returns true if the given input String is a palindrome, false otherwise.
     *
     * @param input the String to check
     */
    public boolean isPalindrome(String input) {
        // TODO Implement a recursive solution to this method.
        if(input.length()>1) {
            return input.charAt(0) == input.charAt(input.length() - 1) &&
                isPalindrome(input.substring(1, input.length() - 1));
        }else{
            return true;
        }
    }
}
